from Data_IO import order_condition
import copy


class Rule:

    # def __init__(self, attributes_dict = {}, decision_dict ={},covering = [], num_data = 0):
    def __init__(self, attributes_dict={}, decision_list=[], covering=[], num_data=0):
        self.num_data = num_data
        self.covering = covering
        self.attributes_dict = attributes_dict
        self.decision_list = decision_list

    # def get_decision(self, case):           #use dict
    #     for value in self.decision_dict.keys():
    #         if case in self.decision_dict[value]:
    #             return value

    def get_decision(self, case):             # use list
        return self.decision_list[case-1]

    def get_conditions(self, case):           # use dict
        conditions = {}
        for attri in self.covering:
            for value in self.attributes_dict[attri]:
                if case in self.attributes_dict[attri][value]:
                    conditions[attri] = value
        return conditions

    # def get_conditions(self, case):         #use table
    #     conditions = {}
    #     for attri in self.covering:
    #         conditions[attri] = da.data[case - 1][da.attributes_name.index(attri)]
    #     return conditions

    def get_cases(self, conditions={}):   # use dict
        data_list = [0] * (self.num_data+1)
        cases = []
        for attri in conditions.keys():
            for case in self.attributes_dict[attri][conditions[attri]]:
                data_list[case] += 1
        for i in range(1, len(data_list)):
            if data_list[i] == len(conditions):
                cases.append(i)
        return cases

    # def get_cases(self, conditions = {}):      # use table
    #     cases = [i for i in range(1, self.num_data +1)]
    #     for attri in conditions.keys():
    #         col = da.attributes_name.index(attri)
    #         temp = copy.copy(cases)
    #         for case in cases:
    #             if da.data[case-1][col] != conditions[attri]:
    #                 temp.remove(case)
    #         cases = temp
    #     return cases

    def get_rules(self):
        ruleset = []
        data_list = [0] * (self.num_data+1)

        for case in range(1, self.num_data+1):
            if data_list[case] == 1:
                continue
            else:
                case_decision = self.get_decision(case)
                if case_decision == "special":
                    continue
                case_conditions = self.get_conditions(case)
                # Drop condition
                Q_con = copy.copy(case_conditions)

                # Drop in order
                for attri in order_condition(case_conditions.keys()):
                    Q_con.pop(attri)
                    if self.Is_rule_consistent(self.get_cases(Q_con), case_decision):
                        case_conditions = copy.copy(Q_con)
                    else:
                        Q_con = copy.copy(case_conditions)
                ruleset.append((case_conditions, case_decision))

                for one in self.get_cases(case_conditions):
                    data_list[one] = 1
        # print(ruleset)
        return ruleset

    def Is_rule_consistent(self, cases=[], decision = ""):
        for case in cases:
            if self.get_decision(case) != decision:
                return False
        return True

