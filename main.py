import Data_IO as da
import partition as par
import get_rules as ru
import inconsistency as deal
from cutpoint import add_cutpoint
import time

# Initialization
attributes_dict = {}
decision_dict = {}
decision_list = []
num_attributes = len(da.attributes_name)

for item in da.attributes_name:
    attributes_dict[item] = {}

# Read data
num = 1
for line in da.data:
    # In case there is line is just a '\n'
    if len(line) < num_attributes:
        break
    for i in range(len(da.attributes_name)):
        if line[i] in attributes_dict[da.attributes_name[i]].keys():
            attributes_dict[da.attributes_name[i]][line[i]].append(num)
        else:
            attributes_dict[da.attributes_name[i]][line[i]] = [num]
    if line[i+1] in decision_dict:
        decision_dict[line[i+1]].append(num)
    else:
        decision_dict[line[i+1]] = [num]
    decision_list.append(line[-1])
    num += 1
num_data = num-1
add_cutpoint(attributes_dict)

# Get the partition of decision
decision_parset = par.Partition(decision_dict, num_data)

# Processing the atributes
attributes_parset = {}
for attribute in attributes_dict.keys():
    attributes_parset[attribute] = par.Partition(attributes_dict[attribute], num_data)

# Printing
print("Number of cases:", num_data)
# print("Attributes_name:", da.attributes_name)
# print("Attributes:", attributes_dict)
# print("Decision:", decision_dict)
# for item in attributes_dict.keys():
#     print(item)
#     attributes_parset[item].print_matrix()

start = time.time()
# Check consistency
partition_A = par.merge_partitions(attributes_parset, num_data)
if partition_A.is_smaller(decision_parset):
# If consistent
    print("! Possible rule set is not shown since it is identical with the certain rule set")
    # get a single global convering
    covering = par.global_covering(da.attributes_name, attributes_parset, decision_parset, num_data)
    # get a ruleset
    ruleset = ru.Rule(attributes_dict, decision_list, covering, num_data).get_rules()
    # Output to file
    output_file = open(da.out_name+".certain.r", 'w')
    output_file.write(da.print_ruleset(ruleset))
    output_file.close()

    # Printing
    print("Global covering:", covering)
    # print(da.print_ruleset(ruleset))
    print("The rule set is saved in",da.out_name+".certain.r")

# Not consistent
else:
    print("Inconsistent.")

    # Get different decision and covering sets of concepts
    possible_dict, certain_dict, possible_list, certain_list =\
        deal.deal_inconsistency(attributes_dict, decision_dict, decision_list, da.attributes_name, num_data)

    # Possible
    output_file = open(da.out_name + ".possible.r", 'w')
    for dp_dict_key in possible_dict.keys():
        dp_dict = possible_dict[dp_dict_key]
        dp_list = possible_list[dp_dict_key]
        dp_parset = par.Partition(dp_dict, num_data)

        # get a single global convering
        covering = par.global_covering(da.attributes_name, attributes_parset, dp_parset, num_data)

        # get ruleset
        ruleset = ru.Rule(attributes_dict, dp_list, covering, num_data).get_rules()

        output_file.write(da.print_ruleset(ruleset))
        # Printing
        print("Possible - concept:", dp_dict_key, "\tA single global covering:",covering)
        # print(da.print_ruleset(ruleset))
    output_file.close()

    # Certain
    output_file = open(da.out_name + ".certain.r", 'w')
    for dc_dict_key in certain_dict.keys():
        dc_dict = certain_dict[dc_dict_key]
        dc_list = certain_list[dc_dict_key]
        dc_parset = par.Partition(dc_dict, num_data)

        # get a single global conering
        covering = par.global_covering(da.attributes_name, attributes_parset, dc_parset, num_data)

        # get ruleset
        ruleset = ru.Rule(attributes_dict, dc_list, covering, num_data).get_rules()

        output_file.write(da.print_ruleset(ruleset))
        # Printing
        print("Certain - concept:", dc_dict_key, "\tA single global covering:", covering)
        # print(da.print_ruleset(ruleset))
    output_file.close()
    print("The rule set is saved in", da.out_name + ".certain.r and",da.out_name + ".possible.r")

end = time.time()
timer = end - start
print("Program took", timer, "seconds.")

# Printing
# print("A*", partition_A.print_matrix())
