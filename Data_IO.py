
data = []
attributes_name = []
decision_name = ""


def read_data(inputfile=None):
    datas = []
    attributesname = []
    decisionname = ""
    try:
        inputfile.readline()
        lines = inputfile.readline().split()
        if lines[0] == "[":
            attributesname = lines[1:len(lines) - 2]
            decisionname = lines[-2]
        for line in input_file.readlines():
            if line[0] == '!':  # Comment start
                break
            datas.append(line.split()[0:])
        return datas, attributesname, decisionname
    except:
        print("Error occurs in reading data.")
        exit()

flag = True
while flag:
    try:
        input_name = input("Please input the name of the input data file, eg:\"test.txt\"\n")
        # input_name = "test.txt"
        input_file = open(input_name, 'r')
        data, attributes_name, decision_name = read_data(input_file)
        flag = False
        input_file.close()
    except:
        print("The input file can not be open.")

out_name = input("Please input the name of the output data file, eg:\"my-data.d\"\n")
# out_name = "test.d"


# sort the conditions
def order_condition(attributes = []):
    # Order the conditions
    order = {}
    orderlist = []
    for attri in attributes:
        order[attributes_name.index(attri)] = attri
    for i in sorted(order.keys()):
        orderlist.append(order[i])
    return orderlist


# generate the output string
def print_ruleset(ruleset=[]):
    string = ""
    for rule in ruleset:
        # Condition
        flag = False

        # Generate the string
        catstr = ""
        for attri in order_condition(rule[0].keys()):
            if flag:
                catstr += " & "
            flag = True
            catstr += "(" + attri +", "+ rule[0][attri] + ")"
        catstr += " -> " +"("+ decision_name +", "+rule[1]+")\n"
            # print("d", decision_name)
            # print("r",rule[1])
        # print (str)
        string += catstr
    return string
